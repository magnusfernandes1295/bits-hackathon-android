package com.infamousbazooka.bitshackathon;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class CaseAdapter extends RecyclerView.Adapter<CaseAdapter.MyViewHolder> {

    private List<Case> caseList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, date, type;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            type = view.findViewById(R.id.type);
            date = view.findViewById(R.id.date);
        }
    }


    public CaseAdapter(List<Case> caseList) {
        this.caseList = caseList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_case, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Case aCase = caseList.get(position);
        holder.title.setText(aCase.getDesc());
    }

    @Override
    public int getItemCount() {
        return caseList.size();
    }
}
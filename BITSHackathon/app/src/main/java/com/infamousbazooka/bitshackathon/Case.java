package com.infamousbazooka.bitshackathon;

import com.google.gson.annotations.SerializedName;

public class Case {
    @SerializedName("car_id")
    private String car_id;
    @SerializedName("case_no")
    private String case_no;
    @SerializedName("case_type")
    private String case_type;
    @SerializedName("desc")
    private String desc;
    @SerializedName("expected")
    private String expected;
    @SerializedName("proirity")
    private String proirity;
    @SerializedName("service_rep")
    private String service_rep;
    @SerializedName("service_rep_contact")
    private String service_rep_contact;
    @SerializedName("status")
    private String status;


    public Case() {
    }

    public String getCar_id() {
        return car_id;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    public String getCase_no() {
        return case_no;
    }

    public void setCase_no(String case_no) {
        this.case_no = case_no;
    }

    public String getCase_type() {
        return case_type;
    }

    public void setCase_type(String case_type) {
        this.case_type = case_type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getExpected() {
        return expected;
    }

    public void setExpected(String expected) {
        this.expected = expected;
    }

    public String getProirity() {
        return proirity;
    }

    public void setProirity(String proirity) {
        this.proirity = proirity;
    }

    public String getService_rep() {
        return service_rep;
    }

    public void setService_rep(String service_rep) {
        this.service_rep = service_rep;
    }

    public String getService_rep_contact() {
        return service_rep_contact;
    }

    public void setService_rep_contact(String service_rep_contact) {
        this.service_rep_contact = service_rep_contact;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

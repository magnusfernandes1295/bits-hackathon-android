package com.infamousbazooka.bitshackathon;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CaseAPI {
    @GET("getcase/{id}")
    Call<CasesResponse> getCases(@Path("id") int id);
}

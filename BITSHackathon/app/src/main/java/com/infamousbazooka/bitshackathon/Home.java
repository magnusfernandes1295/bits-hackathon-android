package com.infamousbazooka.bitshackathon;

import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class Home extends AppCompatActivity {
    public static DrawerLayout mDrawerLayout;
    public static Toolbar toolbar;
    public static NavigationView mNavigationView;
    public static FragmentManager mFragmentManager;
    public static TextView toolTitle;
    public static FragmentTransaction mFragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolTitle = findViewById(R.id.toolTitle);
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mNavigationView = findViewById(R.id.navview) ;
        toolbar = findViewById(R.id.toolbar);
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,new HomeFragment()).commit();

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

                switch (menuItem.getItemId()){
                    case R.id.nav_home:
                        fragmentTransaction.replace(R.id.containerView, new HomeFragment()).commit();
                        return true;
                    case R.id.nav_cases:
                        fragmentTransaction.replace(R.id.containerView, new CasesFragment()).commit();
                        return true;
                    default:
                        Toast.makeText(Home.this, "Error", Toast.LENGTH_SHORT).show();
                        return true;
                }
            }
        });
        mNavigationView.setItemIconTintList(null);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();
    }
}

package com.infamousbazooka.bitshackathon;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CasesResponse {
    @SerializedName("results")
    private List<Case> cases;

    public List<Case> getCases() {
        return cases;
    }
}

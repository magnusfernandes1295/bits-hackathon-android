package com.infamousbazooka.bitshackathon;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CasesFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cases, container, false);
        getCases();
        return view;
    }

    public void getCases() {
        CaseAPI caseAPI =
                APIClient.getClient().create(CaseAPI.class);

        Call<CasesResponse> call = caseAPI.getCases(1001);
        call.enqueue(new Callback<CasesResponse>() {
            @Override
            public void onResponse(Call<CasesResponse> call, Response<CasesResponse> response) {
                List<Case> cases = new ArrayList<>();
                Log.d("TAG", response.body().getCases().toString());
            }

            @Override
            public void onFailure(Call<CasesResponse> call, Throwable t) {
                Log.d("TAG", t.toString());
            }
        });
    }

}
